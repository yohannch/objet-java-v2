package CoteServeur;


import CoteClient.Client;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author renquetc
 */
public class Serveur {
    public static void main(String[] args) throws IOException{

            ServerSocket monServerSocket=null;
            
            try {
                monServerSocket=new ServerSocket(6666);
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-1);
            }
            
            Socket masocket=null;
            try {
                masocket=monServerSocket.accept();
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-2);
            }
            InputStream is=null;
            
            try {
                is=masocket.getInputStream();
            } catch (IOException ex) {
                Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            BufferedInputStream bis=new BufferedInputStream(is);
            
            
            OutputStream os=null;
            
            os=masocket.getOutputStream();
            
            
             
            BufferedOutputStream bos=new BufferedOutputStream(os);
            /////////////////////
            try { 
                for(int i=0;i<5;i++){
                    int octetRecu=(bis.read()*2);
                    //System.out.println(bis.read());
                    System.out.println("L'octet recu est : " + octetRecu);
                    bos.write(octetRecu);
                }
                
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-4);
            }
            /////////////////////

            bos.flush();
    
        try {
            masocket.close();
            monServerSocket.close();
        } catch (Exception e){
            }
        
    }
    
}
