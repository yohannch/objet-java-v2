/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoteClient;

import CoteServeur.Serveur;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author renquetc
 */
public class Client {
    public static void main(String[] args){
        Socket maSocket=null;
        try {
            maSocket=new Socket("127.0.0.1",6666);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
        OutputStream os=null;
        try {
            os=maSocket.getOutputStream();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-2);
        }
        BufferedOutputStream bos=new BufferedOutputStream(os);
        try {
            for(int i=1;i<6;i++){
                bos.write(i);
                //System.out.println(i);
                System.out.println("Octet envoyé avec bonheur");
            }
            
            bos.flush();
            Thread.sleep(10*6);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-3);
        } catch (InterruptedException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }        
        InputStream is=null;
            try {
                is=maSocket.getInputStream();
            } catch (IOException ex) {
                Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
            }
            BufferedInputStream bis=new BufferedInputStream(is);
            /////////////////////
            try { 
                for(int i=0;i<5;i++){
                    int octetRecu=bis.read();
                    //System.out.println(bis.read());
                    System.out.println("L'octet recu est : " + octetRecu);
                }
                
            } catch (IOException ex) {
                ex.printStackTrace();
                System.exit(-4);
            }

        try {
            //bos.flush();
            maSocket.close();
        } catch (Exception e) {
        }
       
    }
    
}
