/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.abstraction;

/**
 *
 * @author != cholleyy
 */
public class Femme extends EtreHumain {
    
    private String nomDeJeuneFille;

    public Femme(String nomDeJeuneFille) {
        super(nomDeJeuneFille);
        this.nomDeJeuneFille = nomDeJeuneFille;
    }

    public String getNomDeJeuneFille() {
        return nomDeJeuneFille;
    }

    @Override
    public void vasTamuser() {
        System.out.println(QuelEstTonNom()+" vas jouer au ballon.");
    }

    @Override
    public String toString() {
        return "Femme{" +
                "nomDeJeuneFille=" + nomDeJeuneFille + 
                "-"+
                super.QuelEstTonNom()+
                '}';
    }
    
    
    
}
