/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.geometrie;

import premierepartiemodule.geometrie.*;
import java.util.logging.Logger;

/**
 * @author renquetc
 * @author cholleyy
 */
public class point {
    private int abscisse;
    private int ordonnee;
    
    /**
     * Déplacement absolue du point à partir de 2 entiers
     * @param x : devient la nouvelle abscisse du point
     * @param ordonnee : devient la nouvelle ordonnee du point
     */
    public void deplacetoi(int x, int ordonnee){
        abscisse=x;
        this.ordonnee=ordonnee;
    }
    /**
     * Déplacement absolue du point à partir des coordonnées d'un autre point b
     * Ne se passe rien si le point b est null
     * @param b : Point b dont les coordonnées seront les nouveaux coordonnées de notre point.
     */
    public void deplacetoi(point b){
        if(b==null) return;
        this.abscisse=b.abscisse;
        this.ordonnee=b.ordonnee;
    }
    /**
     * Constructeur de la classe point
     * @param abscisse : abscisse du point
     * @param ordonnee : ordonee du point
     */
    public point(int abscisse,int ordonnee){
        
        this.abscisse=abscisse;
        this.ordonnee=ordonnee;
    
    }
    public point(){
        this(0,0);
    }
    /*public void afficheToi(){
        System.out.println("abscisse="+abscisse+" - "+"ordonnee="+ordonnee);
    }*/
    @Override
    public String toString(){
        String res="";
        res=("abscisse="+abscisse+" - "+"ordonnee="+ordonnee);
        return res;
    }
    
    public boolean esTuIdentiqueA(point autre){
        if (autre==null)return false;
        if (this==autre)return true;
        else return false;
    }

    public int getAbscisse() {
        return abscisse;
    }

    public void setAbscisse(int abscisse) {
        this.abscisse = abscisse;
    }

    public int getOrdonnee() {
        return ordonnee;
    }

    public void setOrdonnee(int ordonnee) {
        this.ordonnee = ordonnee;
    }
    
    
   
        
}
