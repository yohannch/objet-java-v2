/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.informaticien;


import java.awt.Color;
import secondepartiemodule.geometrie.point;
/**
 *
 * @author bocqc
 */
public class Pixel extends point {
    
    private Color couleur;
    private boolean visible;
    
    
    public Pixel(Color couleur, boolean visible, int abscisse, int ordonnee){
        
        super(abscisse, ordonnee); // super veut dire "superieur", utilise le constructeru de la classe héritée.
        this.couleur = (couleur==null)?Color.BLUE:couleur; //IF-ELSE version courte.
        this.visible = false;
        if(visible)
            allumeToi();
    }
    
    public Pixel(){
        this(null,false,0,0);
    }
    
    public void allumeToi(){
        
        if(!visible){
            /*
            
            C'est ici qu'il faudrait allumer physiquement le Pixel
            
            
            */
            System.out.println("\t Le Pixel est devenu visible.");
            visible = true;
        }
    }
    
    public void eteinsToi(){
        
        if(visible){
            /*
            
            C'est ici qu'il faudrait allumer physiquement le Pixel
            
            
            */
            System.out.println("\t Le Pixel est devenu invisible.");
            visible = false;
        }
    }
    
    

    @Override
    public String toString() {
        return "Pixel{" + "couleur=" + couleur +
                ", visible=" + visible +
                ", " + super.toString()+
                '}';
    }
    
    @Override
    public void deplacetoi(int x, int y){
        
        boolean etatInitial = visible;
        
        if(visible)
            this.eteinsToi();
        
        super.deplacetoi(x,y);
        
        if(etatInitial)
            this.allumeToi();
    }

    @Override
    public void deplacetoi(point b) {
        
        if(b==null) return;
        
        deplacetoi(b.getAbscisse(), b.getOrdonnee());
    }
    
    
    
    
}
