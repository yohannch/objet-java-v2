/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

/**
 *
 * @author cholleyy && renquetc
 */
public interface Demarrable {
    void on();
    void off();
}
