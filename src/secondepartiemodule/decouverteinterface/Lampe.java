/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

/**
 *
 * @author cholleyy
 */
public class Lampe implements Demarrable,Comparable<Lampe> {
    private int puissance;

    public Lampe(int puissance) {
        this.puissance = puissance;
    }

    public int getPuissance() {
        return puissance;
    }

    @Override
    public String toString() {
        return "Lampe{" + "puissance=" + puissance + '}';
    }

    @Override
    public void on() {
         System.out.println("Je suis une Lampe qui s'allume.");
    }

    @Override
    public void off() {
        System.out.println("Je suis une Lampe qui s'éteind.");
    }

    @Override
    public int compareTo(Lampe arg0) {
        return puissance-arg0.puissance;
    }
    
    
}
