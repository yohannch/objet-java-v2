/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

/**
 *
 * @author cholleyy
 */
public class LecteurDVD implements Demarrable,Lecteur{
    private String marque;

    public LecteurDVD(String marque) {
        this.marque = marque;
    }

    public String getMarque() {
        return marque;
    }

    @Override
    public String toString() {
        return "LecteurDVD{" + "marque=" + marque + '}';
    }

    @Override
    public void on() {
         System.out.println("Je suis un Lecteur DVD qui s'allume.");
    }

    @Override
    public void off() {
        System.out.println("Je suis un Lecteur DVD qui s'éteind.");
    }

    @Override
    public void play() {
        System.out.println("Je suis un Lecteur DVD en lecture.");
    }

    @Override
    public void stop() {
        System.out.println("Je suis un Lecteur DVD qui est à l'arrêt.");
    }

    @Override
    public void fastforward() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fastbackward() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
