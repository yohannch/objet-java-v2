/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.decouverteinterface;

import java.util.Comparator;

/**
 *
 * @author cholleyy
 */
public class VieuxComparateur implements Comparator<LecteurDVD> {

    @Override
    public int compare(LecteurDVD arg0, LecteurDVD arg1) {
        return arg0.getMarque().compareTo(arg1.getMarque());
    }
    
}
