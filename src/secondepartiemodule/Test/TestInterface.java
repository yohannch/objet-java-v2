/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.Test;


import java.util.Arrays;
import java.util.Comparator;
import secondepartiemodule.decouverteinterface.Demarrable;
import secondepartiemodule.decouverteinterface.Lampe;
import secondepartiemodule.decouverteinterface.Lecteur;
import secondepartiemodule.decouverteinterface.LecteurDVD;
import secondepartiemodule.decouverteinterface.VieuxComparateur;

/**
 *
 * @author cholleyy
 */
public class TestInterface {
    public static void main(String[] args){
        Lampe laLampe=new Lampe(100);
        laLampe.getPuissance();
        System.out.println(laLampe);
        laLampe.on();
        
        LecteurDVD leLecteurDVD=new LecteurDVD("Hitachi");
        leLecteurDVD.on();
        leLecteurDVD.play();
        System.out.println("----------------------");
        
        Demarrable d;
        d=laLampe;  //          Une lampe est un objet démarrable
        d.on();
        //d.getPuissance()      Le chose démarable ne peuvent pas réstituer leurs puissance
        //d.play();             d n'est pas un lecteur
        
        Lecteur leLecteur=leLecteurDVD;
        leLecteur.play();
        
        System.out.println("----------------------");
        
        Lampe[] desLampes={
            new Lampe(150),
            new Lampe(120),
            new Lampe(50),
            new Lampe(13) 
        };
        
        for(Lampe uneLampe : desLampes)
            System.out.println(uneLampe);
        
        Arrays.sort(desLampes);
        System.out.println("apres tri:");
        
        for(Lampe uneLampe : desLampes)
            System.out.println(uneLampe);
        
        System.out.println("----------------------");
        
        LecteurDVD[] desLecteursDVD={
            new LecteurDVD("Sony"),
            new LecteurDVD("Samsung"),
            new LecteurDVD("Philipps"),
            new LecteurDVD("Moulinex")
    
        };
        
        for(LecteurDVD unLecteurDVD : desLecteursDVD)
            System.out.println(unLecteurDVD);
        
        VieuxComparateur moncomparateur=new VieuxComparateur();
        Arrays.sort(desLecteursDVD,moncomparateur);
        
        System.out.println("apres tri:");
        for(LecteurDVD unLecteurDVD : desLecteursDVD)
            System.out.println(unLecteurDVD);
        
        System.out.println("----------------------");
        
        
        Comparator<LecteurDVD> unPeuPlusModerne= new Comparator<LecteurDVD>() {
            @Override
            public int compare(LecteurDVD arg0, LecteurDVD arg1) {
                return arg1.getMarque().compareTo(arg0.getMarque());            
            }
        };
        
        
        Arrays.sort(desLecteursDVD,unPeuPlusModerne);
        System.out.println("apres tri un peu plus moderne:");
        for(LecteurDVD unLecteurDVD : desLecteursDVD)
            System.out.println(unLecteurDVD);
        
        System.out.println("----------------------");
        
        
        //version "short"
        Arrays.sort(
                desLecteursDVD,
                new Comparator<LecteurDVD>() {
            @Override
            public int compare(LecteurDVD arg0, LecteurDVD arg1) {
                return arg0.getMarque().compareTo(arg1.getMarque());            
            }
        }
                );
        System.out.println("Apres tri court");
        for(LecteurDVD unLecteurDVD : desLecteursDVD)
            System.out.println(unLecteurDVD);
        
        
        
    }
    
}
