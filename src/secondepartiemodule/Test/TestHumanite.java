/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartiemodule.Test;
import secondepartiemodule.abstraction.EtreHumain;
import secondepartiemodule.abstraction.Femme;
import secondepartiemodule.abstraction.Homme;

/**
 *
 * @author <> cholleyy
 */

public class TestHumanite {
    public static void main(String[] args){
        Femme f=new Femme("Simone");
        Homme h=new Homme("Corentin");
        
        System.out.println(f);
        System.out.println(h);
        
        System.out.println(f.getNomDeJeuneFille());
        System.out.println(h.EsTuBarbu());
        
        f.QuelEstTonNom();
        
        System.out.println("-------");
        
        EtreHumain e;
        
        //e=new EtreHumain(); classe abstraite ne pouvant pas etre instanciée
        
        e=f;
        
        //e.GetNomDeJeuneFille(); e n'as pas de methode getnomdejeunefille
        
        System.out.println("-------");
        
        e.vasTamuser();
        
        e=h;
        
        e.vasTamuser();
        
        System.out.println(e.getClass().getName());
        System.out.println(e.getClass().getSuperclass());
        System.out.println(e.getClass().getSuperclass().getSuperclass());
        System.out.println(e.getClass().getSuperclass().getSuperclass().getSuperclass());
    }
}
