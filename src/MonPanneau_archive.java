
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author renquetc
 */
public class MonPanneau_archive extends Panel implements ActionListener{
    private Button leBoutton;
    private Label leLabel;
    private int compteur;
    
    public MonPanneau_archive(){
        leBoutton=new Button("Click ici");
        leLabel = new Label("Tu as cliqué 0 fois !!!");
        
        setLayout(new GridLayout(2,1));
        
        add(leBoutton);
        add(leLabel);
        compteur=0;
        
        //leBoutton.addActionListener(new monecouteur(this));
        leBoutton.addActionListener(this);
        
    }

    void YA_EU_UN_CLICK() {
        compteur++;
        leLabelUpdate("Tu as cliqué " + compteur + " fois !!!");
    }
    
    public void leLabelUpdate(String text){
        leLabel.setText(text);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        YA_EU_UN_CLICK();
    }
    
}
