/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouverteentressorties;

//import java.awt.Point;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import premierepartiemodule.geometrie.point;
//import secondepartiemodule.geometrie.point;

/**
 *
 * @author cholleyy
 */
public class sauverobjet {
    public static void main(String[] args){
    
        ObjectOutputStream oos=null;
        
        try {
            oos=new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream("FicObjet")));
        } catch (FileNotFoundException ex) {
            System.err.println(ex);
            System.exit(-1);
        } catch (IOException ex) {
            System.err.println(ex);
            System.exit(-2);
        }
        
        //String s="Ma belle chaine";
     
       
        point monpoint=new point(2,7);
        try {
            oos.writeObject(monpoint);
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-3);
        }
        
        
        try {
            oos.close();
        } catch (IOException ex) {
            Logger.getLogger(sauverobjet.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    }
}
