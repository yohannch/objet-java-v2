/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouvertemultithreading.premierefacon;

/**
 *
 * @author renquetc
 */
public class TestMonThread {
    public static void main(String[] args) {
        System.out.println("Debit de main");
        MonThread mt1=new MonThread("Toto");
        MonThread mt2=new MonThread("                   Titi");
        mt1.start();
        mt2.start();
        System.out.println("Fin de main");
        try{
        Thread.sleep(10*1000);
        }catch (InterruptedException ex){
        }
        mt1.arreter();
        mt2.arreter();
    }
    
}
