/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouvertemultithreading.secondefacon.decouvertecallable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cholleyy
 */
public class TestCallable {
    public static void main(String[] args) {
        System.out.println("Début de main");
        
        SommeurDEntier somme10=new SommeurDEntier(10);
        SommeurDEntier somme25=new SommeurDEntier(25);
        
        ExecutorService monExecutorService;
        
        monExecutorService=Executors.newFixedThreadPool(3);
        
        Future<Integer> res25 = monExecutorService.submit(somme25);
        
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
        }
        
        Future<Integer> res10 = monExecutorService.submit(somme10);
        
        boolean continuer=true;
        
        while (continuer){
            if (res25.isDone())
                continuer=false;
            else
                try {
                    Thread.sleep(300);
            } catch (InterruptedException ex) {
            }
        }
        
        try {
            Integer leResultat=res25.get();
            System.out.println("Le voilà le résultat : " + leResultat);
        } catch (InterruptedException ex) {
        } catch (ExecutionException ex) {
        }
        
        System.out.println("Fin de main");
    }
    
}
