/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouvertemultithreading.secondefacon;

/**
 *
 * @author cholleyy
 */
public class AutreTest {
    public static void main(String[] args) {
        Runnable r1=new Runnable(){
            @Override
            public void run() {
        
                int compteur=0;
        
                while(true){
                    System.out.println(" - " + compteur + " - " + Thread.currentThread().getName());
                    compteur++;
                    try{
                        Thread.sleep(750);
                    }catch (InterruptedException ex){}
                }
            }
        };
        
        Thread t1=new Thread(r1);
        t1.start();
        
        System.out.println("===============");
        
        Runnable r2=()->{
        int compteur=0;
            while(true){
                System.out.println(" & " + compteur + " - " + Thread.currentThread().getName());
                compteur++;
                try{
                    Thread.sleep(750);
                }catch (InterruptedException ex){}
            }        
        };
        Thread t2=new Thread(r2);
        t2.start();
    
    }
}
