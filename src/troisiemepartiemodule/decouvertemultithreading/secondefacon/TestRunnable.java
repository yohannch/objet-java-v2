/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouvertemultithreading.secondefacon;

/**
 *
 * @author cholleyy
 */
public class TestRunnable {
    public static void main(String[] args) {
        System.out.println("Debut de Programme");
        
        MonRunnable r1=new MonRunnable("Toto");
        MonRunnable r2=new MonRunnable("            Titi");
    
        Thread t1=new Thread(r1);
        Thread t2=new Thread(r2);
        
        t1.start();
        t2.start();
        
        System.out.println("Fin de Programme");
    }
    
}
