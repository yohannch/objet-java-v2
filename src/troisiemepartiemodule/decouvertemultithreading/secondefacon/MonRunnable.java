/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.decouvertemultithreading.secondefacon;

/**
 *
 * @author cholleyy
 */
public class MonRunnable implements Runnable{
    
    private String message;

    public MonRunnable(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        
        int compteur=0;
        
        while(true){
            System.out.println(message + " - " + compteur + " - " + Thread.currentThread().getName());    
            try{
                Thread.sleep(750);    
            }catch (InterruptedException ex){}
            compteur++;
        }
    }
    
    
    
}
