/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartiemodule.ExercicedeSynthèse;

import java.io.Console;
import static java.lang.Math.pow;
import troisiemepartiemodule.decouvertemultithreading.secondefacon.*;

/**
 *
 * @author cholleyy
 */
public class MonRunnable implements Runnable{
    
    private int valeur;
    private int valeurmax;

    public MonRunnable(int valeur,int valeurmax) {
        this.valeur = valeur;
        this.valeurmax= valeurmax;
    }

    public int getValeurmax() {
        return valeurmax;
    }

    public void setValeurmax(int valeurmax) {
        this.valeurmax = valeurmax;
    }

    public void setValeur(int valeur) {
        this.valeur = valeur;
    }

    public int getValeur() {
        return valeur;
    }

    public void runtomax(int maximum) {
        int compteur=0;
        while(compteur<maximum+1){
            compteur+=pow(compteur,0);
            try{
                Thread.sleep(750);
            }catch (InterruptedException ex){}
            System.out.println(valeur);
            setValeur(compteur);
        }
    }
    
    @Override
    public void run() {       
        int compteur=0;
        while(compteur<=valeurmax){
            compteur++;
            try{
                Thread.sleep(750);
            }catch (InterruptedException ex){}
            System.out.println(valeur);
            setValeur(compteur);
        }
    }

}
