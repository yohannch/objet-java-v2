/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premierepartiemodule.geometrie;
import premierepartiemodule.geometrie.point;

/**
 *
 * @author cholleyy
 */
public class droite {
    
    private point p1;
    private point p2;
  
    
    
    
    public droite(point a,point b){
        if(!((a==null) || (b==null) || a.esTuIdentiqueA(b)))
        {
            this.p1=new point(a.getAbscisse(),a.getOrdonnee());
            this.p2=new point(b.getAbscisse(),b.getOrdonnee());
        }
        else{
            this.p1=new point();
            this.p2=new point(1,1);
        }               
    }
    
    public droite(int x1,int y1,int x2,int y2){
        this(new point(x1,y1),new point(x2,y2));
    }
    public droite(){
        
        this.p1=new point();
        this.p2=new point(1,1);
        //this(null,null);
        //this(1,0,0,1);
    }

    public point getP1() {
        return  new point(p1.getAbscisse(),p1.getOrdonnee());
    }

    public void setP1(point p1) {
        if (p1!=null){
            this.p1 = new point(p1.getAbscisse(),p1.getOrdonnee());
        }
        
    }

    public point getP2() {
        return new point(p2.getAbscisse(),p2.getOrdonnee());
    }

    public void setP2(point p2) {
        if (p2!=null){
            this.p2 = new point(p2.getAbscisse(),p2.getOrdonnee());
        }
    }
    public String toString(){
        String res="";
        res=("droite [p1:  " + p1.toString()+"  p2: "+p2.toString()+"]");
        return res;
    }
}
